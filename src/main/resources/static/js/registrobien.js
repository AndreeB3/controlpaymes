$(document).ready(function(){
	
	$('.table-add').click(function () {
		$("#table tbody").append($('<tr>').append(buildFeature())
				.append($('<td>').text($('#featureName').val()))
				.append($('<td>').text($('#featureDescrip').val()))
				.append($('<td>').append('<span class="table-remove glyphicon glyphicon-remove"></span>')));		
	  sortFeature();
	});

	$('.table-remove').click(function () {
	  $(this).parents('tr').detach();
	});
	
	$('#assingCheck').change(function() {
		if ($(this).is(":checked")) {
			$('#boxAsigna').removeClass("hidden");
		} else {
			$('#boxAsigna').addClass("hidden");
		}
	});	
	
	$('#assetType').change(function(){
		$('#assetFamily').find('option').remove().end();
		$('#asassetSubFamily').find('option').remove().end();
		if($(this).val()== 'M'){			
			$('#assetFamily').append('<option value="ME" selected="selected">Mobiliarios y Equipos </option>');
			$('#assetFamily').append('<option value="ET" selected="selected">Equipos de Trasporte </option>');
			$('#assetFamily').append('<option value="HE" selected="selected">Herramientas y Equipos</option>');
			$('#assetFamily').append('<option value="ME" selected="selected">Maquinarias y Equipos</option>');
		}else{
			$('#assetFamily').append('<option value="TE" selected="selected">Terrenos</option>');
			$('#assetFamily').append('<option value="ED" selected="selected">Edificios</option>');
			$('#assetFamily').append('<option value="EM" selected="selected">Edificaciones y Mejoras</option>');
			$('#assetFamily').append('<option value="IN" selected="selected">Instalaciones</option>');
		}
		
	});
	
	$('#assetFamily').change(function(){
		$('#asassetSubFamily').find('option').remove().end();
		if($(this).val()== 'ME'){
			$('#asassetSubFamily').append('<option value="foo" selected="selected">Mobiliario de Oficina</option>');
			$('#asassetSubFamily').append('<option value="foo" selected="selected">Equipo de oficina</option>');
		}else if($(this).val() == 'ET'){
			$('#asassetSubFamily').append('<option value="TE" selected="selected">Transporte de carga</option>');
			$('#asassetSubFamily').append('<option value="ED" selected="selected">Transporte personal</option>');
			$('#asassetSubFamily').append('<option value="EM" selected="selected">Transporte particular</option>');
		}else if($(this).val() == 'TE'){
			$('#asassetSubFamily').append('<option value="TE" selected="selected">Rusticos</option>');
			$('#asassetSubFamily').append('<option value="TE" selected="selected">Urbanos</option>');
		}else{
			$('#asassetSubFamily').append('<option value="TE" selected="selected">-</option>');
		}
		
		
	})
	
	function buildFeature() {
		var id = $('#featureId').val()
		if(id != "" || id != 'undefined'){
			id= 0;
		}
			
		return $("#features").html()
	     .replace("featureId-value",id)
	     .replace("featureName-value",$('#featureName').val())
	     .replace("featureDescrip-value",$('#featureDescrip').val());
	}
	function sortFeature() {		
		$('form .featureId').each(function(index) {
			$(this).attr("name", $(this).attr("name").replace(/index/g, index));
		});
		$('form .featureName').each(function(index) {
			$(this).attr("name", $(this).attr("name").replace(/index/g, index));
		});
		$('form .featureDescrip').each(function(index) {
			$(this).attr("name", $(this).attr("name").replace(/index/g, index));
		});
	}
	
});