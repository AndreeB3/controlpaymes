package com.paymes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ControlPaymesApplication {

	public static void main(String[] args) {
		SpringApplication.run(ControlPaymesApplication.class, args);
	}
}
