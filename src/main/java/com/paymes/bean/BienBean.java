package com.paymes.bean;

import java.sql.Date;
import java.util.List;


public class BienBean {
	
	private String descripcion ;
	private String proveedor; 
	private Date fecha;
	private String tipoBien;
	private String categoriaBien;
	private boolean garantia;
	private int añosGarantia;
	
	private List<CaracteristicaBien> caracteristicas;
	
	/**
	 * @return the caracteristicas
	 */
	public List<CaracteristicaBien> getCaracteristicas() {
		return caracteristicas;
	}
	/**
	 * @param caracteristicas the caracteristicas to set
	 */
	public void setCaracteristicas(List<CaracteristicaBien> caracteristicas) {
		this.caracteristicas = caracteristicas;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getProveedor() {
		return proveedor;
	}
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public String getTipoBien() {
		return tipoBien;
	}
	public void setTipoBien(String tipoBien) {
		this.tipoBien = tipoBien;
	}
	public String getCategoriaBien() {
		return categoriaBien;
	}
	public void setCategoriaBien(String categoriaBien) {
		this.categoriaBien = categoriaBien;
	}
	public boolean isGarantia() {
		return garantia;
	}
	public void setGarantia(boolean garantia) {
		this.garantia = garantia;
	}
	public int getAñosGarantia() {
		return añosGarantia;
	}
	public void setAñosGarantia(int añosGarantia) {
		this.añosGarantia = añosGarantia;
	}
	
	

}
