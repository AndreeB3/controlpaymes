package com.paymes.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.paymes.bean.BienBean;

@Controller
public class InterfaceController {


	@RequestMapping(value="/listabien")
	public ModelAndView registrarBien(){
		ModelAndView model = new ModelAndView();
		model.addObject("viewlist",true);
		model.setViewName("index");
		
		return model;
	}
	
	
	@RequestMapping(value="/formbien")
	public ModelAndView ListarBien(){
		ModelAndView model = new ModelAndView();
		model.addObject("bien",new BienBean());
		model.addObject("form",true);
		model.addObject("title","Registro de Bienes");
		model.setViewName("index");
		
		return model;
	}
}
