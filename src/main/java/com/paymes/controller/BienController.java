package com.paymes.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.paymes.bean.BienBean;
import com.paymes.service.BienService;

@Controller
public class BienController {
	
	@Autowired
	private BienService service;

	
	@RequestMapping(value = "/registrarBien", method = RequestMethod.POST)
	public ModelAndView createNewUser(@Valid BienBean bien, BindingResult bindingResult) {
		ModelAndView model = new ModelAndView();
		String insert = "inserto";
		if(insert.equals("inserto")){
			service.registrarBien(bien);
		}

		model.addObject("viewlist",true);
		model.addObject("title","Lista de Bienes");
		model.setViewName("index");
		
		return model;
	}
}
