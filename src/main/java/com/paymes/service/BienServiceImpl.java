package com.paymes.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.paymes.bean.BienBean;
import com.paymes.model.Bien;
import com.paymes.repository.BienRepository;

@Service("bienService")
public class BienServiceImpl implements BienService {
	
	@Autowired
	private BienRepository bienRepo;
	
	@Override
	public void registrarBien(BienBean b) {
		Bien bien = new Bien();
		bienRepo.save(bien);
		
	}

}
