package com.paymes.service;

import com.paymes.model.User;

public interface UserService {
	
	public User findUserByEmail(String email);
	public void saveUser(User user);
}
