package com.paymes.service;

import org.springframework.stereotype.Service;

import com.paymes.bean.BienBean;

public interface BienService {

	public void registrarBien(BienBean bien);
}
