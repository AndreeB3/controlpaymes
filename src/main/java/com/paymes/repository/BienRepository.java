package com.paymes.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.paymes.model.Bien;
import com.paymes.model.User;

@Repository("bienRepository")
public interface BienRepository extends JpaRepository<Bien, Long> {
	
	

}
