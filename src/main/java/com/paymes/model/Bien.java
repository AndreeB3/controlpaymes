package com.paymes.model;


import java.util.Date;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name= "bien")
public class Bien {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int bienId;
	private String bienDescrip;
	private String proveedor;
	private Date fecha;
	private String bienTipo;
	private String bienCate;
	private double bienCosto;
	private boolean garantia;
	private int añosGarantia;
	/**
	 * @return the bienId
	 */
	public int getBienId() {
		return bienId;
	}
	/**
	 * @param bienId the bienId to set
	 */
	public void setBienId(int bienId) {
		this.bienId = bienId;
	}
	/**
	 * @return the bienDescrip
	 */
	public String getBienDescrip() {
		return bienDescrip;
	}
	/**
	 * @param bienDescrip the bienDescrip to set
	 */
	public void setBienDescrip(String bienDescrip) {
		this.bienDescrip = bienDescrip;
	}
	/**
	 * @return the proveedor
	 */
	public String getProveedor() {
		return proveedor;
	}
	/**
	 * @param proveedor the proveedor to set
	 */
	public void setProveedor(String proveedor) {
		this.proveedor = proveedor;
	}
	/**
	 * @return the fecha
	 */
	public Date getFecha() {
		return fecha;
	}
	/**
	 * @param fecha the fecha to set
	 */
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	/**
	 * @return the bienTipo
	 */
	public String getBienTipo() {
		return bienTipo;
	}
	/**
	 * @param bienTipo the bienTipo to set
	 */
	public void setBienTipo(String bienTipo) {
		this.bienTipo = bienTipo;
	}
	/**
	 * @return the bienCate
	 */
	public String getBienCate() {
		return bienCate;
	}
	/**
	 * @param bienCate the bienCate to set
	 */
	public void setBienCate(String bienCate) {
		this.bienCate = bienCate;
	}
	/**
	 * @return the bienCosto
	 */
	public double getBienCosto() {
		return bienCosto;
	}
	/**
	 * @param bienCosto the bienCosto to set
	 */
	public void setBienCosto(double bienCosto) {
		this.bienCosto = bienCosto;
	}
	/**
	 * @return the garantia
	 */
	public boolean isGarantia() {
		return garantia;
	}
	/**
	 * @param garantia the garantia to set
	 */
	public void setGarantia(boolean garantia) {
		this.garantia = garantia;
	}
	/**
	 * @return the añosGarantia
	 */
	public int getAñosGarantia() {
		return añosGarantia;
	}
	/**
	 * @param añosGarantia the añosGarantia to set
	 */
	public void setAñosGarantia(int añosGarantia) {
		this.añosGarantia = añosGarantia;
	}
	
	
}
